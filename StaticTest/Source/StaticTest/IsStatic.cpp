// Fill out your copyright notice in the Description page of Project Settings.

#include "IsStatic.h"

int AIsStatic::k = 0;
// Sets default values
AIsStatic::AIsStatic()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void AIsStatic::BeginPlay()
{
	Super::BeginPlay();
	UE_LOG(LogTemp, Warning, TEXT("\"k\" is %d"), AIsStatic::k);
	if (AIsStatic::k == 0)
		AIsStatic::k = 20;
	UGameplayStatics::OpenLevel(this, "Spheres", true, "lol");
}

// Called every frame
void AIsStatic::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

