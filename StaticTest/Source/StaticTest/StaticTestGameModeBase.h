// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "sio_client.h"
#include "string"
#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "StaticTestGameModeBase.generated.h"

/**
 * 
 */
UCLASS()
class STATICTEST_API AStaticTestGameModeBase : public AGameModeBase
{
	GENERATED_BODY()

		AStaticTestGameModeBase();
		~AStaticTestGameModeBase();
private:
	sio::client client;
};
