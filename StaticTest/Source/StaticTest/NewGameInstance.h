// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "sio_client.h"
#include "CoreMinimal.h"
#include "Engine/GameInstance.h"
#include "Kismet/GameplayStatics.h"
#include "NewGameInstance.generated.h"

/**
 * 
 */
UCLASS()
class STATICTEST_API UNewGameInstance : public UGameInstance
{
	GENERATED_BODY()

public:
	virtual void Init() override;
	virtual void Shutdown() override;
	void swapScene(sio::event &);
	static void swapLevel(FString name);
	static UNewGameInstance *getCurrentGameInstance();
	static FString swName;

private:
	sio::client client;
	static UNewGameInstance *p_inst;
};
