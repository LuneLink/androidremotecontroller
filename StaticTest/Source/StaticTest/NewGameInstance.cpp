// Fill out your copyright notice in the Description page of Project Settings.

#include "NewGameInstance.h"

UNewGameInstance *UNewGameInstance::p_inst = 0;
FString UNewGameInstance::swName = "";

void UNewGameInstance::swapScene(sio::event & ev)
{
	const std::string msg = ev.get_message()->get_string();
	FString str2(UTF8_TO_TCHAR(msg.c_str()));
	FName name = FName(*str2);
	UGameplayStatics::OpenLevel(this, name, true, "lol");
	UE_LOG(LogTemp, Warning, TEXT("level name %s"), *str2);
}

UNewGameInstance *UNewGameInstance::getCurrentGameInstance()
{
	return UNewGameInstance::p_inst;
}

void UNewGameInstance::Init()
{
	UGameInstance::Init();

	UNewGameInstance::swName = "";
	if (!UNewGameInstance::getCurrentGameInstance())
	{
		UE_LOG(LogTemp, Warning, TEXT("NULL"));
	}
	UNewGameInstance::p_inst = this;
	client.connect("http://192.168.1.113:8089");
	std::string name("Unreal");
	client.socket()->emit("setClientType", name);
	if (UNewGameInstance::getCurrentGameInstance())
	{
		UE_LOG(LogTemp, Warning, TEXT("NOT NULL"));
	}
	//client.socket()->on("scene", std::bind(&UNewGameInstance::swapScene, this, std::placeholders::_1, std::placeholders::_2));


	client.socket()->on("scene", [&](sio::event & ev)
	{
		const std::string msg = ev.get_message()->get_string();
		FString str2(UTF8_TO_TCHAR(msg.c_str()));

		UNewGameInstance::swapLevel(str2);
		//UGameplayStatics::OpenLevel(UNewGameInstance::getCurrentGameInstance(), name, true, "lol");
		//UGameplayStatics::OpenLevel(this, name, true, "lol");
		/*
		//
		//UGameplayStatics::OpenLevel(UNewGameInstance::getCurrentGameInstance(), name, true, "lol");
		UE_LOG(LogTemp, Warning, TEXT("level name %s"), *str2);*/
	});

	UE_LOG(LogTemp, Warning, TEXT("Init GameInstance"));
}


void UNewGameInstance::Shutdown()
{
	UE_LOG(LogTemp, Warning, TEXT("Game Shutdown"));
}

void UNewGameInstance::swapLevel(FString name)
{
	if (UNewGameInstance::swName.Len() == 0 && (UNewGameInstance::swName.Compare(name) != 0))
	{
		UNewGameInstance::swName = name;
		UE_LOG(LogTemp, Warning, TEXT("level name %s"), *UNewGameInstance::swName);
	}
	//UGameplayStatics::OpenLevel(UNewGameInstance::p_inst, name, true, "lol");
}