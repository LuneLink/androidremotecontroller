"use strict";

var _config = require("config");

var _config2 = _interopRequireDefault(_config);

var _socket = require("socket.io");

var _socket2 = _interopRequireDefault(_socket);

var _express = require("express");

var _express2 = _interopRequireDefault(_express);

var _http = require("http");

var _http2 = _interopRequireDefault(_http);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var app = (0, _express2.default)();
var server = _http2.default.createServer(app);
var io = new _socket2.default(server);

app.get('/', function (req, res) {
	res.send('<h1>Hello world</h1>');
});

io.on('connect', function (socket) {
	console.log(socket.id);
	socket.on('new message', function (msg) {
		console.log(msg);
	});
});

server.listen(_config2.default.get('port'), function () {
	console.log('Express server listening on port ' + _config2.default.get('port'));
});
//# sourceMappingURL=server.js.map