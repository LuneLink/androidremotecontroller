'use strict';

var _socket = require('socket.io-client');

var _socket2 = _interopRequireDefault(_socket);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var socket = _socket2.default.connect('http://127.0.0.1:8089');

socket.on('connect', function () {
	console.log('Successfully connected!');
});
//# sourceMappingURL=client.js.map