import io from 'socket.io-client';
let socket = io.connect('http://127.0.0.1:8089');

socket.on('connect', () => {
	console.log('Successfully connected!');
	socket.emit("setClientType", "Unreal");
	socket.on("scene", (name) => {
		console.log(name);
	})
});
