import config from "config";
import SocketIO from "socket.io";
import express from 'express';
import http from 'http';

let app = express();
let server = http.createServer(app);
let io = new SocketIO(server);
let UnrealClient = undefined;

app.get('/', function(req, res){
	res.send('<h1>Hello world</h1>');
});

io.on('connect', (socket) =>
{
    socket.on('setClientType', (type) =>{
        socket.clientType = type;
        if (type === "Unreal")
		{
			if (UnrealClient !== undefined)
			{
				if (io.sockets.sockets[UnrealClient].connected == true)
				{
					console.log("refuse Unreal");
					socket.disconnect();
					return;
				}
			}
            UnrealClient = socket.id;
			socket.on('disconnect', function () {
				console.log("disconnected");
					//socket.emit('disconnected');
				UnrealClient = undefined;
			});
		}
        console.log("Id:" + socket.id + " Type: " + socket.clientType);
    });

	socket.on('new message', function (msg)
	{
		if (UnrealClient !== undefined)
            io.sockets.sockets[UnrealClient].emit("scene", msg);
	});
});

server.listen(config.get('port'),() =>{
	console.log('Express server listening on port '+ config.get('port'));
});