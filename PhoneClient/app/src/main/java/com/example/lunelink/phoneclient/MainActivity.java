package com.example.lunelink.phoneclient;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import android.widget.Button;

import java.net.URISyntaxException;

import io.socket.client.IO;
import io.socket.client.Socket;


public class MainActivity extends AppCompatActivity {

    private TextView textView;
    private static final String ADDR = "http://192.168.1.113:8089";
    private Socket socket;
    private static final String TAG = "myLogs";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        {
            try {
                socket = IO.socket(ADDR);
                Log.d(TAG , "connected!");
            } catch (URISyntaxException e) {
                Log.d(TAG , "no connection");
            }
        }
        Log.d(TAG , "onCReate _myfu");
        socket.connect();
        socket.emit("setClientType", "Android");
    }

    public void onClick(View view) {
        socket.emit("new message", ((Button) view).getText().toString());
    }
}
